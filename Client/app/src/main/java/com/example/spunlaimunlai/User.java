package com.example.spunlaimunlai;

public class User {
    private String idUser;
    private String firstName;
    private String lastName;
    private String age;
    private String description;
    private String username;
    private String password;

    public String getIdUser() {
        return idUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAge() {
        return age;
    }

    public String getDescription() {
        return description;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
