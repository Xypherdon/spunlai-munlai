package com.example.spunlaimunlai;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolderApi {

    @GET("api/v1/users")
    Call<List<User>> getUsers();
}
